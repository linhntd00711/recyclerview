package com.example.recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements  ProductAdapter.IOnClickItem {
    List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //B1: data source
        initData();

        //B2: Adapter
        ProductAdapter adapter = new ProductAdapter(this,listProduct,this);

        //B3: LayoutManaer
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);

        //B4 RecycleView
        RecyclerView rvProduct = (RecyclerView) findViewById(R.id.rvProduct);

        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData(){
        listProduct.add(new Product("Zara P1","Love Shift","111111",R.drawable.p1));
        listProduct.add(new Product("Zara P2","Love Shift","2222",R.drawable.p2));
        listProduct.add(new Product("Zara P3","Love Shift","333",R.drawable.p3));
        listProduct.add(new Product("Zara P4","Love Shift","4444",R.drawable.p4));
        listProduct.add(new Product("Zara P5","Love Shift","55555",R.drawable.p5));
        listProduct.add(new Product("Zara P6","Love Shift","66666",R.drawable.p6));

    }

    //handle when click item RecycleView
    @Override
    public void onClickItem(int position){
        Product product= listProduct.get(position);
        Toast.makeText(this,product.getTitle(),Toast.LENGTH_SHORT).show();
    }
}
